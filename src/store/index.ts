import Vue from "vue";
import Vuex from "vuex";

import axios from "axios";
Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    bookdata: [] as any,
    startIndex: 0,
    query: {
      q: "",
      filter: "",
      orderBy: "newest",
      printType: "all",
      langRestrict: "",
      isRange: false,
      range: [0, 1000000],
    },
    isFavarite: false,
  },
  mutations: {
    setData(state, data) {
      state.query = { ...state.query, ...data };
    },
    setFavarite(state, data) {
      state.isFavarite = data;
    },
  },
  actions: {
    async loadBook({ state }, input) {
      if (input.isLoadmore) {
        state.startIndex = state.startIndex + 10;
      } else {
        state.startIndex = 0;
      }

      let path = `https://www.googleapis.com/books/v1/volumes?q=${state.query.q}&orderBy=${state.query.orderBy}&startIndex=${state.startIndex}&printType:${state.query.printType}`;

      if (state.query.filter) {
        path += `&filter=${state.query.filter}`;
      }
      if (state.query.langRestrict) {
        path += `&langRestrict=${state.query.langRestrict}`;
      }

      const targetBookData = await axios.get(path).then(({ data }) => {
        if (state.query.isRange) {
          data.items = data.items.filter(
            (x: any) =>
              x.saleInfo.listPrice &&
              x.saleInfo.listPrice.amount >= state.query.range[0] * 31 &&
              x.saleInfo.listPrice.amount <= state.query.range[1] * 31
          );
        }

        if (input.isLoadmore) {
          state.bookdata = [...state.bookdata, ...data.items];
        } else {
          state.bookdata = data.items;
        }
        return data;
      });

      return targetBookData;
    },
  },
  modules: {},
  getters: {
    getbookdata(state) {
      return state.bookdata;
    },
    getIsFavarite(state) {
      return state.isFavarite;
    },
  },
});
